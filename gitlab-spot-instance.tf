variable "ssh_key_name" { description = "Name of AWS ssh key create ahead of time." }
variable "aws_region_name" { description = "AWS region name i.e. us-west-2" }
variable "instance_type" { default = "m5.xlarge" }

provider "aws" {
  # Use keys in home dir.
  #  access_key = "ACCESS_KEY_HERE"
  #  secret_key = "SECRET_KEY_HERE"
  region = var.aws_region_name
}

data "external" "myipaddr" {
  program = ["bash", "-c", "curl -s 'https://api.ipify.org?format=json'"]
}

data "aws_ami" "gitlab" {
  most_recent = true

  filter {
    name   = "name"
    values = ["GitLab EE*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["782774275127"] # GitLab
}

# Request a spot instance at $1.00
# Master Node
resource "aws_spot_instance_request" "gitlab" {
  wait_for_fulfillment = true
  count                = "1"
  ami                  = data.aws_ami.gitlab.id
  spot_price           = "1.00"
  instance_type        = var.instance_type
  spot_type            = "one-time"

  vpc_security_group_ids = [aws_security_group.gitlab_sg.id]

  key_name = var.ssh_key_name

  user_data = <<EOT
#!/bin/bash
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash
export PUBHOSTNAME=(`curl http://169.254.169.254/latest/meta-data/public-hostname`)
export LOCALIPV4=(`curl http://169.254.169.254/latest/meta-data/local-ipv4`)
sed -i 's@^\s*external_url.*@'"external_url 'http://$PUBHOSTNAME'"'@' /etc/gitlab/gitlab.rb
gitlab-ctl reconfigure
gitlab-ctl restart
sleep 300 # This is for a problem with apt file locking
apt-get update
apt-get install docker.io gitlab-runner -y
export REGISTRATION_TOKEN=(`gitlab-rails runner -e production " \
puts Gitlab::CurrentSettings.current_application_settings.runners_registration_token"`)
gitlab-runner register \
--non-interactive \
--url "http://$LOCALIPV4/" \
--registration-token "$REGISTRATION_TOKEN" \
--executor "docker" \
--docker-image alpine:latest \
--description "docker-runner" \
--tag-list "docker,aws" \
--run-untagged="true" \
--locked="false" \
--access-level="not_protected"
EOT
  tags = {
    Name = "GitLab EE"
    App  = "GitLab"
  }
}

resource "aws_security_group" "gitlab_sg" {

}

resource "aws_security_group_rule" "allow_all_egress" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "all"
  cidr_blocks = ["0.0.0.0/0"]
  description = "Outbound access to ANY"

  security_group_id = aws_security_group.gitlab_sg.id
}


resource "aws_security_group_rule" "allow_all_myip" {
  type        = "ingress"
  from_port   = 0
  to_port     = 0
  protocol    = "all"
  cidr_blocks = ["${data.external.myipaddr.result["ip"]}/32"]
  description = "Single IP Access to Server"

  security_group_id = aws_security_group.gitlab_sg.id
}


output "master_ip" {
  value = aws_spot_instance_request.gitlab.0.public_ip
}

output "instance_id" {
  value = aws_spot_instance_request.gitlab.0.spot_instance_id
}

output "root_login" {
  value = "Username: root\nPassword: ${aws_spot_instance_request.gitlab.0.spot_instance_id}"
}