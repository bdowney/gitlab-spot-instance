# Terraform Script to Install GitLab from AMI

Terraform script to spin up latest GitLab from published AMI on a Spot Instance in AWS.

1. Install terraform from https://www.terraform.io/downloads.html
1. Update `variable "ssh_key_name"` with your ssh key name from the correct region or define it in a [terraform.tfvars](https://www.terraform.io/docs/configuration/variables.html#variable-definitions-tfvars-files) file.
1. Ensure your [AWS access-keys are accessable to terraform](https://www.terraform.io/docs/providers/aws/index.html#environment-variables).

If you want to open up IP access to the server, you need to add/edit the security group settings. By default terraform will attempt to get your workstation's public IP address and open up all ports to that single IP. 


```sh
terraform init
terraform plan
terraform apply -var ssh_key_name=your-key-name
```

You will get an output of the public IP address of the new server, it will take a few minutes to start up. Access via `http://public_ip`

You may receive a 502 error while it's starting up, just wait a moment longer.

You should get the `Create New Password` screen.

A single shared runner should be automaticly added to the server after about 10 minutes. There is an issue in the startup script that prevents it from being installed too quickly.